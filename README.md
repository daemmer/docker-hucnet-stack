Simple Docker-Stack with Reverse-Proxy and DNS (-Filter)
---

My minimal and basic services for my private home network. This provided docker compose stack consists of a reverse proxy (https://traefik.io/), a local DNS (https://pi-hole.net/) and the initial proxy network creation.

## Deployment steps

1. create and customise your environment-file (`.env`-file)
2. create the `acme.json`-file and apply the right permissions
3. create the basic `traefik.toml`-file
4. `docker-compose up -d`

## Configurations

### A Basic Traefik `traefik.toml` Example

https://docs.traefik.io/basics/

```bash
[entryPoints]
  [entryPoints.http]
  address = ":80"
  [entryPoints.https]
  address = ":443"
    [entryPoints.https.tls]

# If set to true invalid SSL certificates are accepted for backends.
# This disables detection of man-in-the-middle attacks so should only be used on secure backend networks.
#
# Optional
# Default: false
#
insecureSkipVerify = true

# ACME (Let's Encrypt): automatic SSL.
[acme]
email = "admin@yourdomain"
storage = "acme.json"
onDemand = true
#onHostRule = true
acmeLogging = true
entryPoint = "https"

# CA server to use.
# Uncomment the line to use Let's Encrypt's staging server,
# leave commented to go to prod.
#
# Optional
# Default: "https://acme-v02.api.letsencrypt.org/directory"
#
#caServer = "https://acme-staging-v02.api.letsencrypt.org/directory"

  [acme.httpChallenge]
  entryPoint = "http"
```

## Your environment variables file (`.env`)

https://docs.docker.com/compose/env-file/

```env
#!/usr/bin/env
# Environment variables for Compose
# Source: https://docs.docker.com/compose/environment-variables/#the-env-file
#

HOST_IP=<IP>

# container dns servers
DNS1=9.9.9.9
DNS2=8.8.8.8

TRAEFIK_VERSION=latest
PIHOLE_VERSION=latest
PIHOLE_WEBPASSWORD=Swordfish

```

## Create ACME (Let's Encrypt) Configuration Storage File (`acme.json`)

https://docs.traefik.io/configuration/acme/

```bash
touch acme.json
chmod 600

```
